package edTareaBlog_Liher;

public class Post {
	private int id;
	private String contenido;
	private String titulo;
	private String autor;
	
	public Post(int id,String titulo, String contenido, String autor) {
		this.id = id;
		this.titulo = titulo;
		this.contenido = contenido;
		this.autor = autor;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String showPost() {
		return "Post id:"+ this.id + " [Autor=" + this.autor + "] " + "[Titulo=" + this.titulo +"] "+"[text=" + contenido + "] ";
	}
	public String toString() {
		return "Post [text=" + contenido + "]";
	}
}