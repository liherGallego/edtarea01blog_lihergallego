package edTareaBlog_Liher;

import java.util.Scanner;
public class Main {
	private static Blog newBlog = new Blog();
	public static void main(String[] args) {
		
		String option ="";
		
		// Ejecutar la opci�n seleccionada
		do {
			menu();
			System.out.println("Elige una opcion: ");
			Scanner reader = new Scanner(System.in);
			
			option = reader.nextLine();
			stateMachine(option);
			
		}while(!option.equals("5"));
	}
	
	private static void menu() {
		// Mostrar un men�
				System.out.println("----Welcome to Blog Manager----");
				System.out.println("Elige una opcion:");
				System.out.println("1. A�adir post");
				System.out.println("2. Eliminar post");
				System.out.println("3. Mostrar post");
				System.out.println("4. Mostrar los primeros 10 posts del blog");
				System.out.println("5. Salir");
	}
	
	private static void stateMachine(String option) {
		Scanner reader = new Scanner(System.in);
		String id ="";
		String index = "";
		String titulo = "";
		String contenido = "";
		String autor = "";
		switch (option) {
		
			case "1":
				System.out.println("Introduce el n�mero id del post: ");
				id = reader.nextLine();
				System.out.println("Introduce el t�tulo del post: ");
				titulo = reader.nextLine();
				System.out.println("Introduce el contenido del post: ");
				contenido = reader.nextLine();
				System.out.println("Introduce el autor del post: ");
				autor = reader.nextLine();
				Post newPost = new Post(Integer.parseInt(id),titulo,contenido,autor);
				System.out.println("Introduce el index del post (orden de aparici�n en el blog): ");
				index = reader.nextLine();
				System.out.println(newBlog.AddPost(newPost, Integer.parseInt(index)));
				break;		
			case "2":
				System.out.println("Introduce el n�mero id del post: ");
				id = reader.nextLine();
				System.out.println(newBlog.remove(Integer.parseInt(id)));
				break;
			case"3":
				System.out.println("Introduce el n�mero id del post: ");
				id = reader.nextLine();
				if (newBlog.getPost(Integer.parseInt(id)) == null) {
					System.out.println("NO EXISTE EL POST");
				}
				System.out.println(newBlog.getPost(Integer.parseInt(id)).showPost());
				break;
			case"4":
				// Se recorre con getPostbyIndex porque queremos los 10 primeros (importa index no id)
				System.out.println("Estos son los 10 primeros posts: ");
				for (int i=0; i<10; i++) {
					System.out.println(newBlog.getPostbyIndex(i).toString());
				}
				break;
			default:
				break;
			}
		}
}

