package edTareaBlog_Liher;

import java.util.ArrayList;

public class Blog {
	
	private ArrayList <Post> posts;
	
	public Blog() {
		this.posts = new ArrayList<Post>();
	}
	
	public Post getPost(int id) {
		
		for (Post post : posts) {
			if(post.getId() == id) {
				return post;
			}
		}
		return null;
	}	
	
	public Post getPostbyIndex(int index) {
		
		return posts.get(index);		
	}	
	public String AddPost(Post newPost,int index){
		for (Post post : posts) {
			if(post.getId() == newPost.getId()) {
				return "YA EXISTE UN POST CON ESTE ID";
			}
		}
		posts.add(index-1, newPost);
		return "POST A�ADIDO!!!";
	}
	
	public String remove(int id) {
		for (Post post : posts) {
			if(post.getId() == id){
				posts.remove(post);
				return "ELIMINADO!!!";
			}
		}
			return "NO SE HA ENCONTADO EL POST!!!";
	}
}
